#Istio demo #https://docs.microsoft.com/en-us/azure/aks/istio-scenario-routing

```

https://docs.microsoft.com/en-us/azure/aks/istio-install

ISTIO_VERSION=1.0.4
curl -sL "https://github.com/istio/istio/releases/download/$ISTIO_VERSION/istio-$ISTIO_VERSION-osx.tar.gz" | tar xz
cd istio-\$ISTIO_VERSION
```

#helm

```
kubectl create serviceaccount -n kube-system tiller
kubectl create clusterrolebinding tiller --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller
```

#fix for kiali and grafana

```
k apply -f https://aka.ms/kiali-grafana-fix
```

#password is 1f2d1e2e67df

#istio

```
helm install install/kubernetes/helm/istio --name istio --namespace istio-system \
 --set global.controlPlaneSecurityEnabled=true \
 --set grafana.enabled=true \
 --set tracing.enabled=true \
 --set kiali.enabled=true
```

#access dashboards

```
kubectl port-forward -n istio-system $(kubectl get pod -n istio-system -l app=kiali -o jsonpath='{.items[0].metadata.name}') 20001:20001 &
kubectl port-forward -n istio-system $(kubectl get pod -n istio-system -l app=jaeger -o jsonpath='{.items[0].metadata.name}') 16686:16686 &
kubectl -n istio-system port-forward $(kubectl -n istio-system get pod -l app=prometheus -o jsonpath='{.items[0].metadata.name}') 9090:9090 &
kubectl -n istio-system port-forward $(kubectl -n istio-system get pod -l app=grafana -o jsonpath='{.items[0].metadata.name}') 3000:3000 &
```

```
open http://localhost:20001 &
open http://localhost:16686 &
open http://localhost:9090 &
open http://localhost:3000 &
```

#Mark default namespace for istio injection

```
kubectl label namespace default istio-injection=enabled
kubectl get namespace -L istio-injection
```

#Deploy the bookinfo app

```
kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml
kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
```

#get the ingress IP

```
kubectl get svc istio-ingressgateway -n istio-system

kubectl apply -f samples/bookinfo/networking/destination-rule-all-mtls.yaml
kubectl apply -f samples/bookinfo/networking/virtual-service-all-v1.yaml

kubectl apply -f samples/bookinfo/networking/virtual-service-reviews-test-v2.yaml
kubectl apply -f samples/bookinfo/networking/virtual-service-reviews-50-v3.yaml
kubectl apply -f samples/bookinfo/networking/virtual-service-reviews-v3.yaml
kubectl delete -f samples/bookinfo/networking/virtual-service-all-v1-mtls.yaml
```

#load it up

```
docker run fortio/fortio load -qps 100 -c 50 -t 300s http://23.97.152.130/
```

#aks-voting-app

```
kubectl create namespace voting
kubectl label namespace voting istio-injection=enabled

kubectl apply -f kubernetes/step-1-create-voting-app.yaml --namespace voting
istioctl create -f istio/step-1-create-voting-app-gateway.yaml --namespace voting

kubectl apply -f kubernetes/step-2-update-voting-analytics-to-1.1.yaml --namespace voting
INGRESS_IP=
for i in {1..50}; do curl -si $INGRESS_IP | grep results; done
```

#lock to v1.1

```
istioctl replace -f istio/step-2a-update-voting-app-virtualservice.yaml --namespace voting
istioctl create -f istio/step-2b-add-routing-for-all-components.yaml --namespace voting
```

#canary

```
istioctl replace -f istio/step-3-add-routing-for-2.0-components.yaml --namespace voting
kubectl apply -f kubernetes/step-3-update-voting-app-with-new-storage.yaml --namespace voting
```
